-- MySQL Script generated by MySQL Workbench
-- Tue Feb 22 20:27:23 2022
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema myfitness
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema myfitness
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `myfitness` DEFAULT CHARACTER SET utf8 ;
USE `myfitness` ;

-- -----------------------------------------------------
-- Table `myfitness`.`Administrador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myfitness`.`Administrador` (
  `idAdministrador` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(65) NOT NULL,
  `clave` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idAdministrador`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myfitness`.`Marca`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myfitness`.`Marca` (
  `idMarca` INT NOT NULL,
  `marca` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idMarca`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myfitness`.`TipoProducto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myfitness`.`TipoProducto` (
  `idTipoProducto` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idTipoProducto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myfitness`.`Producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myfitness`.`Producto` (
  `idProducto` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `precio` VARCHAR(45) NOT NULL,
  `cantidad` VARCHAR(45) NOT NULL,
  `foto` VARCHAR(45) NULL,
  `Marca_idMarca` INT NOT NULL,
  `TipoProducto_idTipoProducto` INT NOT NULL,
  `Administrador_idAdministrador` INT NOT NULL,
  PRIMARY KEY (`idProducto`),
  INDEX `fk_Producto_Marca_idx` (`Marca_idMarca` ASC) VISIBLE,
  INDEX `fk_Producto_TipoProducto1_idx` (`TipoProducto_idTipoProducto` ASC) VISIBLE,
  INDEX `fk_Producto_Administrador1_idx` (`Administrador_idAdministrador` ASC) VISIBLE,
  CONSTRAINT `fk_Producto_Marca`
    FOREIGN KEY (`Marca_idMarca`)
    REFERENCES `myfitness`.`Marca` (`idMarca`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Producto_TipoProducto1`
    FOREIGN KEY (`TipoProducto_idTipoProducto`)
    REFERENCES `myfitness`.`TipoProducto` (`idTipoProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Producto_Administrador1`
    FOREIGN KEY (`Administrador_idAdministrador`)
    REFERENCES `myfitness`.`Administrador` (`idAdministrador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myfitness`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myfitness`.`Cliente` (
  `idCliente` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(45) NOT NULL,
  `telefono` VARCHAR(45) NOT NULL,
  `Producto_idProducto` INT NOT NULL,
  `direccion` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`idCliente`),
  INDEX `fk_Cliente_Producto1_idx` (`Producto_idProducto` ASC) VISIBLE,
  CONSTRAINT `fk_Cliente_Producto1`
    FOREIGN KEY (`Producto_idProducto`)
    REFERENCES `myfitness`.`Producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myfitness`.`Domicilio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myfitness`.`Domicilio` (
  `idDomicilio` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(45) NOT NULL,
  `Cliente_idCliente` INT NOT NULL,
  PRIMARY KEY (`idDomicilio`),
  INDEX `fk_Domicilio_Cliente1_idx` (`Cliente_idCliente` ASC) VISIBLE,
  CONSTRAINT `fk_Domicilio_Cliente1`
    FOREIGN KEY (`Cliente_idCliente`)
    REFERENCES `myfitness`.`Cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
